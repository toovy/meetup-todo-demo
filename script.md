# create todo
- create-todo component erstellen
- newTodoText als prop einführen
- tagName auf nix
- li.list-group-item.create-todo
- li.list-group-item > h5 {{model.title}}
- onsubmit auf createTodo, newTodoText mitgeben
- show: createTodo als routeAction mit Liste
- ApplicationRoute: createTodo implementieren
  - createRecord
  - pushObject
  - 2x save (item,list)

# show todo
- component todo-list takes items
- component todo-list-item takes item

# open/done


# update app hbs

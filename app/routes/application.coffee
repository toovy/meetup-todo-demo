ApplicationRoute = Ember.Route.extend

  model: ->
    @get('store').findAll 'todo-list'

  actions:
    createTodo: (list, text) ->
      item = @get('store').createRecord('todo-item', { text: text, list: list })
      list.get('items').pushObject item
      item.save().then () ->
        list.save()
      return false

`export default ApplicationRoute`

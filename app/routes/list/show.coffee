ShowRoute = Ember.Route.extend

  model: (params) ->
    @get('store').peekRecord 'todo-list', params.id

  actions:
    delete: ->
      decision = confirm 'Really delete?'
      @currentModel.destroyRecord()
      @transitionTo '/'

`export default ShowRoute`

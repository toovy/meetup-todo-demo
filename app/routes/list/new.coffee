NewRoute = Ember.Route.extend

  model: ->
    @get('store').createRecord 'todo-list'

  actions:

    willTransition: ->
      if @currentModel.get('isNew')
        @currentModel.destroyRecord()

    submit: ->
      @currentModel.save().then =>
        @transitionTo 'list.show', @currentModel

    cancel: ->
      @currentModel.destroyRecord()
      @transitionTo '/'

`export default NewRoute`

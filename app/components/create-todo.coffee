import Ember from 'ember'

export default Ember.Component.extend

  tagName: ''

  newTodoText: ''

  actions:
    refresh: ->
      @set 'newTodoText', ''
      return false

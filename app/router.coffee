import Ember from 'ember';
import config from './config/environment';

Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map ->
  @route 'list', ->
    @route 'new'
    @route 'show', path: ':id/show'


export default Router;

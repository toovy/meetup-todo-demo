import DS from 'ember-data'

export default DS.Model.extend
  title: DS.attr 'string'
  items: DS.hasMany 'todo-item'

  doneTodos: Ember.computed.filterBy 'items', 'done', true
  openTodos: Ember.computed.filterBy 'items', 'done', false

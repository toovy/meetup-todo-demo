import DS from 'ember-data'

export default DS.Model.extend
  text: DS.attr 'string'
  list: DS.belongsTo 'todo-list'
  done: DS.attr 'boolean', defaultValue: false

  doneChanged: Ember.observer 'done', ->
    @save()
